/* eslint-disable no-await-in-loop */

const { GroupAddress } = require('knxnetip');
const GlobalCacheManager = require('gc-adapter');
const get = require('lodash/get');

const { delay } = require('./util');

const gc = new GlobalCacheManager();

module.exports = async ({
  ack,
  message,
  knxnetip,
  subscribe,
  unsubscribe,
  sock,
}) => {
  try {
    console.debug('[DEBUG] routing %s %s', sock.id, message.type);
    switch (message.type) {
      case 'read': {
        const address = GroupAddress.new(message.address);
        const { cemi } = await knxnetip.readRequest({ address, timeout: 25000 });
        if (ack) { ack(null, cemi.toString('hex')); }
        break;
      }
      case 'write': {
        const address = GroupAddress.new(message.address);
        const { data } = message;
        await knxnetip.writeRequest({
          address,
          data: typeof data === 'number' ? data : Buffer.from(message.data, 'hex'),
        });
        if (ack) { ack(null, true); }
        break;
      }
      case 'subscribe': {
        const address = GroupAddress.new(message.address);
        subscribe(`${address}`);
        if (ack) { ack(null, true); }
        break;
      }
      case 'unsubscribe': {
        const address = GroupAddress.new(message.address);
        unsubscribe(`${address}`);
        if (ack) { ack(null, true); }
        break;
      }
      case 'exec': {
        // eslint-disable-next-line no-restricted-syntax
        for (const command of message.commands) {
          if ('id' in command || 'address' in command) {
            const address = GroupAddress.new(command.id || command.address);
            const data = typeof command.data === 'undefined' ? command.value : command.data;
            const buf = typeof data === 'number' ? data : Buffer.from(command.data, 'hex');
            await knxnetip.writeRequest({ address, data: buf });
          }
          if ('command' in command) {
            await gc.sendir({
              host: get(command, 'device.host', command.host),
              port: get(command, 'device.port', command.port),
              output: get(command, 'device.output', command.output),
              command: command.command,
            });
          }
          await delay(command.delay);
        }
        if (ack) { ack(null, true); }
        break;
      }
      default: break;
    }
  } catch (caughtErr) {
    if (ack) { ack(caughtErr); }
    console.error('[ERROR] %s', caughtErr.message);
  }
};


module.exports = {
  stringify: obj => JSON.stringify(obj, null, 2),
  async delay(ms) {
    if (typeof ms === 'number') {
      await new Promise(resolve => setTimeout(resolve, ms));
    }
  },
};

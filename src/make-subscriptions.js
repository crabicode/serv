const WebSocket = require('ws');
const { stringify } = require('./util');

module.exports = ({ sock, emitter }) => {
  const subscriptions = {};
  const subscribe = (event) => {
    // проверим существует ли подписка
    if (subscriptions[event]) { return false; }
    // если подписка не существует, добавляем её в бродкастинг событий
    const handler = (...args) => {
      if (sock.readyState === WebSocket.CLOSING || sock.readyState === WebSocket.CLOSED) {
        return;
      }
      sock.send(stringify({ event, args }));
    };
    // подписываемся и сохраняем колбэк
    emitter.on(event, handler);
    subscriptions[event] = handler;
    console.debug('[DEBUG] %s added subscription %s', sock.id, event);
    return true;
  };

  const unsubscribe = (event) => {
    const handler = subscriptions[event];
    if (handler) {
      // если подписка была то удаляем подписку и удаляем листенер
      emitter.removeListener(event, handler);
      delete subscriptions[event];
      console.debug('[DEBUG] %s removed subscription', sock.id, event);
      return true;
    }
    return false;
  };

  const destroy = () => {
    Object.keys(subscriptions).forEach((event) => {
      const handler = subscriptions[event];
      emitter.removeListener(event, handler);
      delete subscriptions[event];
    });
  };

  return { subscribe, unsubscribe, destroy };
};

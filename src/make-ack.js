const WebSocket = require('ws');
const { stringify } = require('./util');

module.exports = (sock, message) => {
  let pending = null;
  return async (err, data) => {
    if (pending === null) {
      pending = new Promise((resolve) => {
        const callback = (caughtErr) => {
          if (caughtErr) {
            resolve(false);
            return;
          }
          resolve(true);
        };
        const payload = err
          ? { id: message.id, error: err.message }
          : { id: message.id, data };
        if (sock.readyState === WebSocket.CLOSING || sock.readyState === WebSocket.CLOSED) {
          resolve(false);
          return;
        }
        sock.send(stringify(payload), callback);
      });
      return pending;
    }
    await pending;
    return false;
  };
};

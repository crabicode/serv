const WebSocket = require('ws');
const { stringify } = require('./util');

module.exports = ({ sock }) => value => new Promise((resolve, reject) => {
  if (value && typeof value === 'object') {
    if (sock.readyState === WebSocket.CLOSING || sock.readyState === WebSocket.CLOSED) {
      throw new Error('sock closed');
    }
    const handleResolve = (caughtErr) => {
      if (caughtErr) {
        reject(caughtErr);
        return;
      }
      resolve();
    };
    const data = stringify(value);
    sock.send(data, handleResolve);
  }
  throw new Error('Invalid argument');
});

const WebSocket = require('ws');
const EventEmitter = require('events');
const { Connection } = require('knxnetip');
const Koa = require('koa');
const proxy = require('koa-better-http-proxy');
const cors = require('@koa/cors');

let sockid = 0;
const makeAck = require('./src/make-ack');
const makeEmit = require('./src/make-emit');
const makeSubscriptions = require('./src/make-subscriptions');
const route = require('./src/route-message');

process.on('unhandledRejection', (caughtErr) => {
  throw caughtErr;
});

const app = new Koa();
const wss = new WebSocket.Server({ port: 9000 });
const knx = new Connection({ host: '192.168.1.250', port: 3671 });
const emitter = new EventEmitter();

app.use(cors());
app.use(proxy('http://192.168.1.250/', { headers: { Authorization: 'Basic YWRtaW46dGVzdDA4MDg=' } }));

async function main() {
  knx.on('message', async (caughtErr, message, sender) => {
    if (caughtErr) { return; }
    if (typeof message.apci !== 'undefined' && message.channel === knx.channel) {
      const addr = `${message.dst}`;
      const data = message.cemi.toString('hex');
      const senderAddr = `${message.src}`;
      const operation = ['reads', 'responds', 'writes'][message.apci];

      console.debug(
        '[DEBUG] Received %s %s:%s %s %s %s to %s',
        sender.family,
        sender.address,
        sender.port,
        senderAddr,
        operation,
        data,
        addr,
      );

      if (message.apci === 0x01 || message.apci === 0x02) {
        emitter.emit(addr, { data, sender: senderAddr });
        emitter.emit('*', { addr, data, sender: senderAddr });
      }
    }
  });

  await knx.open();

  app.listen(9001);

  // Handle connection

  wss.on('connection', async (sock) => {
    Object.assign(sock, { id: ++sockid });
    console.log('[INFO] %s', 'connected client', sock.id);

    const emit = makeEmit({ sock });
    const { subscribe, unsubscribe, destroy } = makeSubscriptions({ sock, emitter });

    sock.on('message', async (str) => {
      try {
        const message = JSON.parse(str);
        if (message && typeof message === 'object') {
          const ack = 'id' in message ? makeAck(sock, message) : null;
          try {
            route({
              ack,
              sock,
              emit,
              message,
              subscribe,
              unsubscribe,
              knxnetip: knx,
            });
          } catch (caughtErr) {
            if (ack) {
              ack(caughtErr.message);
            }
            // прокинуть ошибку дальше
            throw caughtErr;
          }
        }
      } catch (caughtErr) {
        console.error('[ERROR] %s error %s', sock.id, caughtErr.message);
      }
    });

    sock.on('close', () => {
      destroy();
      console.debug('[INFO] flushed subscriptions for %s', sock.id);
      console.log('[INFO] %s disconnected', sock.id);
    });
  });
}

if (process.env.NODE_ENV !== 'test') {
  const [, ...args] = process.argv;
  main.call(null, ...args);
}
